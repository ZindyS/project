package com.example.strukovgym;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

public class Main2Activity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);
    }

    //Считываем нажатие на надпись и на фон
    public void onSMTClicked(View v) {
        Intent intent = new Intent(Main2Activity.this, Step_3.class);
        startActivity(intent);
        finish();
    }
}
