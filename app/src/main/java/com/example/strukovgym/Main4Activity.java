package com.example.strukovgym;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

public class Main4Activity extends AppCompatActivity {

    EditText ed1, ed2;

    //Создание SharedPreferences
    SharedPreferences sh;
    SharedPreferences.Editor ed;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main4);

        //Задание айдишек
        ed1 = findViewById(R.id.weight);
        ed2 = findViewById(R.id.height);

        //Настройка SharedPreferences
        sh = getSharedPreferences("Bruh", 0);
        ed = sh.edit();
    }

    //Считываем нажатия
    public void onClicked(View v) {
        //Проверка на пустоту полей
        if (!TextUtils.isEmpty(ed1.getText()) && !TextUtils.isEmpty(ed2.getText())) {
            //Переход на след активити и созранение значений
            ed.putString("weight", String.valueOf(ed1.getText()));
            ed.putString("height", String.valueOf(ed2.getText()));
            ed.commit();
            Intent intent = new Intent(Main4Activity.this, Step_3.class);
            startActivity(intent);
        } else {
            Toast.makeText(this, "Заполните поля!", Toast.LENGTH_SHORT).show();
        }
    }
}
