package com.example.strukovgym;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;

public class Step_2 extends AppCompatActivity {

    LinearLayout male, female;
    Button button;
    int bool;

    //Создание SharedPreferences
    SharedPreferences sh;
    SharedPreferences.Editor ed;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_step_2);

        //Задание айдишек
        male = findViewById(R.id.male);
        female = findViewById(R.id.female);
        button = findViewById(R.id.button2);

        //Настройка кнопок
        female.setBackgroundResource(R.drawable.buttom_no);
        male.setBackgroundResource(R.drawable.buttom_no);
        button.setBackgroundResource(R.drawable.buttom_no);

        //Вспомог. переменная
        bool = 0;

        //Настройка SharedPreferences
        sh = getSharedPreferences("Bruh", 0);
        ed = sh.edit();

        //Считывание нажатий, изменение цвета и значения вспомог. переменной
        female.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                female.setBackgroundResource(R.drawable.button_yes);
                male.setBackgroundResource(R.drawable.buttom_no);
                button.setBackgroundResource(R.drawable.button_yes);
                bool = 2;
            }
        });
        male.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                female.setBackgroundResource(R.drawable.buttom_no);
                male.setBackgroundResource(R.drawable.button_yes);
                button.setBackgroundResource(R.drawable.button_yes);
                bool = 1;
            }
        });
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (bool != 0){ //Если одна из кнопок нажата, то открываем Step_3
                    ed.putInt("pol",  bool);
                    ed.commit();
                    Intent in = new Intent(Step_2.this,Step333.class);
                    startActivity(in);
                    finish();
                }
            }
        });
    }
}
