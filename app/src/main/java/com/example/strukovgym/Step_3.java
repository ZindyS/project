package com.example.strukovgym;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class Step_3 extends AppCompatActivity {

    //Если есть проблемы с CLEARTEXT, то в манифесте нужно прописать android:usesCleartextTraffic="true"

    //В ПРИЛОЖЕНИИ ДЛЯ МОБИЛКИ ЕСТЬ ОШИБКА В РЕГИСТРАЦИИ И АВТОРИЗАЦИИ (на я хз в чём она заключается (я даже не уверен, вдруг у вас всё сработает, а я буду плакац))!

    EditText pas,log;
    Button avto;
    TextView reg;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_step_3);
        pas = findViewById(R.id.email);
        log = findViewById(R.id.user);
        avto = findViewById(R.id.button4);
        reg = findViewById(R.id.sign_up);

        //Пересылает на регестрацию
        reg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(Step_3.this,Step_3_reg.class);
                startActivity(in);
                finish();
            }
        });
    }

    //Считывание нажатия
    public void onClick (View view){
        // Проверка на заполненность полей
        if(TextUtils.isEmpty(pas.getText())||TextUtils.isEmpty(log.getText())){
            Toast.makeText(getApplicationContext(),"Заполните поля",Toast.LENGTH_LONG).show();
        }else{ //Если поля не пустые делаем вызов Retrofit
            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl("http://gym.areas.su/")
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
            final Interface api = retrofit.create(Interface.class);
            //Запрос
            api.cal(new Messe(String.valueOf(pas.getText()),String.valueOf(log.getText()))).enqueue(new Callback<MesseClass>() {
                @Override
                public void onResponse(Call<MesseClass> call, Response<MesseClass> response) {
                    if(response.isSuccessful()){
                        String str = response.body().notice.getAnswer();
                        if(!TextUtils.isEmpty(response.body().notice.getToken())){
                            //Переход на след. активность
                            Intent intent = new Intent(Step_3.this, Main2Activity.class);
                            startActivity(intent);
                        } else if (!TextUtils.isEmpty(response.body().notice.getAnswer())) {
                            //Проверка на то, активен ли пользователь
                            if (str.equals("User is active")) {
                                //Запрос
                                api.cal3(new Messe(String.valueOf(log.getText()))).enqueue(new Callback<MesseClass>() {
                                    @Override
                                    public void onResponse(Call<MesseClass> call, Response<MesseClass> response) {
                                        if (response.isSuccessful()) {
                                            //Запрос
                                            api.cal(new Messe(String.valueOf(pas.getText()),String.valueOf(log.getText()))).enqueue(new Callback<MesseClass>() {
                                                @Override
                                                public void onResponse(Call<MesseClass> call, Response<MesseClass> response) {
                                                    if (response.isSuccessful()) {
                                                        if (!TextUtils.isEmpty(response.body().notice.getToken())) {
                                                            //Переход на след. активность
                                                            Intent intent = new Intent(Step_3.this, Main2Activity.class);
                                                            startActivity(intent);
                                                        } else {
                                                            Toast.makeText(Step_3.this, "Error!", Toast.LENGTH_SHORT).show();
                                                        }
                                                    } else {
                                                        Toast.makeText(Step_3.this, response.message(), Toast.LENGTH_SHORT).show();
                                                    }
                                                }

                                                @Override
                                                public void onFailure(Call<MesseClass> call, Throwable t) {
                                                    Toast.makeText(getApplicationContext(), t.getMessage(), Toast.LENGTH_LONG).show();
                                                }
                                            });
                                        } else {
                                            Toast.makeText(Step_3.this, response.message(), Toast.LENGTH_SHORT).show();
                                        }
                                    }

                                    @Override
                                    public void onFailure(Call<MesseClass> call, Throwable t) {
                                        Toast.makeText(Step_3.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                                    }
                                });
                            } else {
                                Toast.makeText(Step_3.this, response.body().notice.getAnswer(), Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            Toast.makeText(Step_3.this, response.body().notice.getText(), Toast.LENGTH_SHORT).show();
                        }
                    }else{
                        Toast.makeText(getApplicationContext(),response.message(),Toast.LENGTH_LONG).show();
                    }
                }

                @Override
                public void onFailure(Call<MesseClass> call, Throwable t) {
                    Toast.makeText(getApplicationContext(), t.getMessage(),Toast.LENGTH_LONG).show();
                }
            });

        }
    }
}
