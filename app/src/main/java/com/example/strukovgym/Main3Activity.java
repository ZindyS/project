package com.example.strukovgym;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class Main3Activity extends AppCompatActivity {

    Button adv, keen, newb;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main3);

        //Задание айдишек
        adv = findViewById(R.id.adv);
        keen = findViewById(R.id.keen);
        newb = findViewById(R.id.newb);

        //Настройка кнопок
        adv.setBackgroundResource(R.drawable.buttom_step3);
        keen.setBackgroundResource(R.drawable.buttom_step3);
        newb.setBackgroundResource(R.drawable.buttom_step3);

        //Считываем нажатия, настраиваем кнопки и переходим на другое активити
        adv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                adv.setBackgroundResource(R.drawable.button_yes);
                keen.setBackgroundResource(R.drawable.buttom_step3);
                newb.setBackgroundResource(R.drawable.buttom_step3);
                Intent intent = new Intent(Main3Activity.this, Main4Activity.class);
                startActivity(intent);
                finish();
            }
        });
        keen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                keen.setBackgroundResource(R.drawable.button_yes);
                adv.setBackgroundResource(R.drawable.buttom_step3);
                newb.setBackgroundResource(R.drawable.buttom_step3);
                Intent intent = new Intent(Main3Activity.this, Main4Activity.class);
                startActivity(intent);
                finish();
            }
        });
        newb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                newb.setBackgroundResource(R.drawable.button_yes);
                adv.setBackgroundResource(R.drawable.buttom_step3);
                keen.setBackgroundResource(R.drawable.buttom_step3);
                Intent intent = new Intent(Main3Activity.this, Main4Activity.class);
                startActivity(intent);
                finish();
            }
        });
    }
}
