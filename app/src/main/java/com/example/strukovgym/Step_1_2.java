package com.example.strukovgym;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.Button;

public class Step_1_2 extends AppCompatActivity {
    Button bt1,bt2,bt3;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_step_1_2);

        //Задание айдишек
        bt1 = findViewById(R.id.button);
        bt2 = findViewById(R.id.button2);
        bt3 = findViewById(R.id.button3);

        //Настройка кнопок
        bt1.setBackgroundResource(R.drawable.buttom_no);
        bt2.setBackgroundResource(R.drawable.buttom_no);
        bt3.setBackgroundResource(R.drawable.buttom_no);

        //Считывание нажатий, изменение цвета кнопок, переход на след. активность
        bt1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bt1.setBackgroundResource(R.drawable.button_yes);
                bt2.setBackgroundResource(R.drawable.buttom_no);
                bt3.setBackgroundResource(R.drawable.buttom_no);
                Intent in = new Intent(Step_1_2.this,Step_2.class);
                startActivity(in);
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        bt1.setBackgroundResource(R.drawable.buttom_no);
                        bt2.setBackgroundResource(R.drawable.buttom_no);
                        bt3.setBackgroundResource(R.drawable.buttom_no);
                    }
                }, 200);
            }
        });
        bt2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bt1.setBackgroundResource(R.drawable.buttom_no);
                bt2.setBackgroundResource(R.drawable.button_yes);
                bt3.setBackgroundResource(R.drawable.buttom_no);
                Intent in = new Intent(Step_1_2.this,Step_2.class);
                startActivity(in);
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        bt1.setBackgroundResource(R.drawable.buttom_no);
                        bt2.setBackgroundResource(R.drawable.buttom_no);
                        bt3.setBackgroundResource(R.drawable.buttom_no);
                    }
                }, 200);
            }
        });
        bt3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bt1.setBackgroundResource(R.drawable.buttom_no);
                bt2.setBackgroundResource(R.drawable.buttom_no);
                bt3.setBackgroundResource(R.drawable.button_yes);
                Intent in = new Intent(Step_1_2.this,Step_2.class);
                startActivity(in);
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        bt1.setBackgroundResource(R.drawable.buttom_no);
                        bt2.setBackgroundResource(R.drawable.buttom_no);
                        bt3.setBackgroundResource(R.drawable.buttom_no);
                    }
                }, 200);
            }
        });

    }
}
