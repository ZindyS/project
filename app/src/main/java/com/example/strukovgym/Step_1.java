package com.example.strukovgym;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

 class Step_1 extends AppCompatActivity {

    Button bt1,bt2;
    TextView step,text;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_step_1);

        //Задание айдишек
        bt1 = findViewById(R.id.button);
        bt2 = findViewById(R.id.button2);
        step = findViewById(R.id.textView);
        text = findViewById(R.id.textView2);

        //Начальное задание айдишек
        bt1.setBackgroundResource(R.drawable.buttom_no);
        bt2.setBackgroundResource(R.drawable.buttom_no);
    }

    //Считывание нажатий, изменение цвета кнопки и переход на след. активность (с задержкой для изменения цвета кнопок на стандартное)
    public void onClick1 (View view){
        bt2.setBackgroundResource(R.drawable.buttom_no);
        bt1.setBackgroundResource(R.drawable.button_yes);
        Intent in = new Intent(Step_1.this,Step_1_2.class);
        startActivity(in);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                bt1.setBackgroundResource(R.drawable.buttom_no);
                bt2.setBackgroundResource(R.drawable.buttom_no);
            }
        }, 200);
    }
    public void onClick2 (View view){
        bt1.setBackgroundResource(R.drawable.buttom_no);
        bt2.setBackgroundResource(R.drawable.button_yes);
        Intent in = new Intent(Step_1.this,Step_1_2.class);
        startActivity(in);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                bt1.setBackgroundResource(R.drawable.buttom_no);
                bt2.setBackgroundResource(R.drawable.buttom_no);
            }
        }, 200);
    }
}
