package com.example.strukovgym;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;

public class Step333 extends AppCompatActivity {

    Button arm, leg, spin, tor;

    //Создание SharedPreferences
    SharedPreferences sh;

    //Создание анимации
    Animation anim;

    ImageView im;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_step_333);

        //Задание айдишек
        arm = findViewById(R.id.arm);
        leg = findViewById(R.id.legs);
        spin = findViewById(R.id.spine);
        tor = findViewById(R.id.torso);
        im = findViewById(R.id.imag);

        //Начальная настройка кнопок
        arm.setBackgroundResource(R.drawable.buttom_step3);
        leg.setBackgroundResource(R.drawable.buttom_step3);
        spin.setBackgroundResource(R.drawable.buttom_step3);
        tor.setBackgroundResource(R.drawable.buttom_step3);

        //Настройка SharedPreferences
        sh = getSharedPreferences("Bruh", 0);

        //Задание картинки, в зависимости от выбранного пола
        if(sh.getInt("pol", 1) == 2) {
            im.setBackgroundResource(R.drawable.icons8female64);
        } else {
            im.setBackgroundResource(R.drawable.icons8male64);
        }

        //Настройка анимации
        anim = AnimationUtils.loadAnimation(this, R.anim.animama);
        anim.setDuration(500);
        im.setAnimation(anim);

        //Считывание кнопок, настройка кнопок и переход на след активити
        arm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                arm.setBackgroundResource(R.drawable.button_yes);
                leg.setBackgroundResource(R.drawable.buttom_step3);
                spin.setBackgroundResource(R.drawable.buttom_step3);
                tor.setBackgroundResource(R.drawable.buttom_step3);
                Intent intent = new Intent(Step333.this, Main3Activity.class);
                startActivity(intent);
                finish();
            }
        });
        spin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                spin.setBackgroundResource(R.drawable.button_yes);
                leg.setBackgroundResource(R.drawable.buttom_step3);
                arm.setBackgroundResource(R.drawable.buttom_step3);
                tor.setBackgroundResource(R.drawable.buttom_step3);
                Intent intent = new Intent(Step333.this, Main3Activity.class);
                startActivity(intent);
                finish();
            }
        });
        tor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tor.setBackgroundResource(R.drawable.button_yes);
                leg.setBackgroundResource(R.drawable.buttom_step3);
                spin.setBackgroundResource(R.drawable.buttom_step3);
                arm.setBackgroundResource(R.drawable.buttom_step3);
                Intent intent = new Intent(Step333.this, Main3Activity.class);
                startActivity(intent);
                finish();
            }
        });
        leg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                leg.setBackgroundResource(R.drawable.button_yes);
                arm.setBackgroundResource(R.drawable.buttom_step3);
                spin.setBackgroundResource(R.drawable.buttom_step3);
                tor.setBackgroundResource(R.drawable.buttom_step3);
                Intent intent = new Intent(Step333.this, Main3Activity.class);
                startActivity(intent);
                finish();
            }
        });
    }
}
