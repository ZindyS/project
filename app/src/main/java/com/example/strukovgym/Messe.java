package com.example.strukovgym;

import com.google.gson.annotations.SerializedName;

public class Messe {
    @SerializedName("username")
    String user;
    @SerializedName("password")
    String pas;
    @SerializedName("email")
    String email;
    @SerializedName("token")
    String token;
    @SerializedName("answer")
    String answer;
    @SerializedName("text")
    String text;
    @SerializedName("weight")
    String weight;
    @SerializedName("height")
    String height;

    public Messe (String pas,String user){
        this.user = user;
        this.pas = pas;
    }
    public Messe (String pas,String user,String email, String weight, String height){
        this.user = user;
        this.pas = pas;
        this.email = email;
        this.weight = weight;
        this.height = height;
    }

    public Messe(String user) {
        this.user = user;
    }


    public String getToken() {
        return token;
    }

    public String getAnswer() {
        return answer;
    }

    public String getText() {
        return text;
    }
}
