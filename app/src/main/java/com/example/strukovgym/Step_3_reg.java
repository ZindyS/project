package com.example.strukovgym;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class Step_3_reg extends AppCompatActivity {

    //Если есть проблемы с CLEARTEXT, то в манифесте нужно прописать android:usesCleartextTraffic="true"

    //В ПРИЛОЖЕНИИ ДЛЯ МОБИЛКИ ЕСТЬ ОШИБКА В РЕГИСТРАЦИИ И АВТОРИЗАЦИИ (на я хз в чём она заключается (я даже не уверен, вдруг у вас всё сработает, а я буду плакац))!

    EditText pas,log,email,pas2;
    Boolean bool;

    //Создание SharedPreferences
    SharedPreferences sh;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_step_3_reg);

        //Задание айдишек
        log = findViewById(R.id.user);
        email = findViewById(R.id.email);
        pas = findViewById(R.id.pas);
        pas2 = findViewById(R.id.pas2);

        //Вспомог. переменная
        bool = false;

        //Настройка SharedPreferences
        sh = getSharedPreferences("Bruh", 0);
    }

    //Считывание нажатий
    public void onClickk (View view) {
        // Проверка на заполненность полей
        if (TextUtils.isEmpty(pas.getText()) || TextUtils.isEmpty(log.getText())) {
            Toast.makeText(getApplicationContext(), "Заполните поля", Toast.LENGTH_LONG).show();
        } else {
            //Записал в переменные, т.к. оно по-другому не работает :D
            String pass1 = String.valueOf(pas.getText());
            String pass2 = String.valueOf(pas2.getText());
            if (pass1.equals(pass2)) { // Сравнение 1 и 2 пароля

                char[] ch = String.valueOf(email.getText()).toCharArray();      //Проверка на @
                for (int i = 0; i < ch.length; i++) {
                    if (ch[i] == '@') {
                        bool = true;
                        break;
                    }
                }
                if (bool) {         //Если @ есть делаем вызов Retrofit
                    Retrofit retrofit = new Retrofit.Builder()
                            .baseUrl("http://gym.areas.su/")
                            .addConverterFactory(GsonConverterFactory.create())
                            .build();
                    Interface api = retrofit.create(Interface.class);
                    //Запрос
                    api.cal2(new Messe(String.valueOf(pas.getText()), String.valueOf(log.getText()), String.valueOf(email.getText()), sh.getString("weight", "1"), sh.getString("height", "1"))).enqueue(new Callback<MesseClass>() {
                        @Override
                        public void onResponse(Call<MesseClass> call, Response<MesseClass> response) {
                            if (response.isSuccessful()) {
                                if(!TextUtils.isEmpty(response.body().notice.getToken())){
                                    Toast.makeText(Step_3_reg.this, "Success!", Toast.LENGTH_SHORT).show();
                                    //Переход на след. активность
                                    Intent intent = new Intent(Step_3_reg.this, Step_3.class);
                                    startActivity(intent);
                                } else if (!TextUtils.isEmpty(response.body().notice.getAnswer())) {
                                    Toast.makeText(Step_3_reg.this, response.body().notice.getAnswer(), Toast.LENGTH_SHORT).show();
                                } else {
                                    Toast.makeText(Step_3_reg.this, response.body().notice.getText(), Toast.LENGTH_SHORT).show();
                                }
                            } else {
                                Toast.makeText(getApplicationContext(), response.message(), Toast.LENGTH_LONG).show();
                            }
                        }

                        @Override
                        public void onFailure(Call<MesseClass> call, Throwable t) {
                            Toast.makeText(getApplicationContext(), t.getMessage(), Toast.LENGTH_LONG).show();
                        }
                    });

                } else {        //Если пароли не совпадают
                    Toast.makeText(getApplicationContext(), "Введите правильный формат Email'а", Toast.LENGTH_LONG).show();
                }
            } else {
                Toast.makeText(getApplicationContext(), "Пароли не совпадают!", Toast.LENGTH_LONG).show();
            }

        }
    }
}
