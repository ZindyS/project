package com.example.myapplication3;


import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.wearable.activity.WearableActivity;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class Main2Activity extends WearableActivity {

    //Создание Retrofit
    Retrofit retrofit = new Retrofit.Builder()
            .baseUrl("http://gym.areas.su/")
            .addConverterFactory(GsonConverterFactory.create())
            .build();

    //Создание SharedPreferences
    SharedPreferences sh;

    TextView proc, water;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);

        //Настройка SharedPreferences
        sh = getSharedPreferences("Bruh", 0);

        //Задание айдишек
        water = findViewById(R.id.water);
        proc = findViewById(R.id.procent);

        //Настройка текстов (я принял надпись в макете "0 ml of 24 I0" за "0 ml of 2410", т.к. оно по смыслу подходит)
        water.setText(String.valueOf(sh.getInt("water", 0)) + " ml of 24 I0");
        proc.setText(String.valueOf(sh.getInt("water", 0) * 100 /2410) + "%");
    }

    //Считываем нажатие
    public void onJuiceOrWaterCl(View v) {
        Intent intent = new Intent(Main2Activity.this, Main3Activity.class);
        startActivity(intent);
        finish();
    }

    //Считываем нажатие
    public void onClickedSignOut(View v) {
        Interface api = retrofit.create(Interface.class);
        //Запрос
        api.cal3(new Messe(String.valueOf(sh.getString("name", "Userr")))).enqueue(new Callback<MesseClass>() {
            @Override
            public void onResponse(Call<MesseClass> call, Response<MesseClass> response) {
                if (response.isSuccessful()) {
                    Intent intent = new Intent(Main2Activity.this, MainActivity.class);
                    startActivity(intent);
                } else {
                    Toast.makeText(Main2Activity.this, response.message(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<MesseClass> call, Throwable t) {
                Toast.makeText(Main2Activity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }
}
