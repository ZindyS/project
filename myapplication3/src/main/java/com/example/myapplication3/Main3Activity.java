package com.example.myapplication3;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.wearable.activity.WearableActivity;
import android.view.View;
import android.widget.EditText;

public class Main3Activity extends WearableActivity {

    EditText editText;

    //Создание SharedPreferences
    SharedPreferences sh;
    SharedPreferences.Editor ed;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main3);

        //Задание айдишки
        editText = findViewById(R.id.editText);

        //Настройка SharedPreferences
        sh = getSharedPreferences("Bruh", 0);
        ed = sh.edit();
    }

    //Считывание нажатий
    public void onCancelClicked(View v) {
        Intent intent = new Intent(Main3Activity.this, Main2Activity.class);
        startActivity(intent);
        finish();
    }

    //Считывание нажатия
    public void onAddClicked(View v) {
        //Сохранение значение (к старому значению добавляем то, что написал пользователь)
        ed.putInt("water", sh.getInt("water", 0) + Integer.valueOf(String.valueOf(editText.getText())));
        ed.commit();
        Intent intent = new Intent(Main3Activity.this, Main2Activity.class);
        startActivity(intent);
        finish();
    }
}
