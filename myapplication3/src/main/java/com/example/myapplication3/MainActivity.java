package com.example.myapplication3;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.wearable.activity.WearableActivity;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MainActivity extends WearableActivity {

    EditText pas,log;

    //Создание SharedPreferences
    SharedPreferences sh;
    SharedPreferences.Editor ed;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Присваивание айдишек
        log = findViewById(R.id.user);
        pas = findViewById(R.id.pas);

        //Настройка SharedPreferences
        sh = getSharedPreferences("Bruh", 0);
        ed = sh.edit();

        // Enables Always-on
        setAmbientEnabled();
    }

    //Считывание нажатий
    public void onClick (View view) {
        // Проверка на заполненность полей
        if (TextUtils.isEmpty(pas.getText()) || TextUtils.isEmpty(log.getText())) {
            Toast.makeText(getApplicationContext(), "Заполните поля", Toast.LENGTH_LONG).show();
        } else { //Если поля не пустые делаем вызов Retrofit
            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl("http://gym.areas.su/")
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
            final Interface api = retrofit.create(Interface.class);
            //Запрос
            api.cal(new Messe(String.valueOf(pas.getText()), String.valueOf(log.getText()))).enqueue(new Callback<MesseClass>() {
                @Override
                public void onResponse(Call<MesseClass> call, Response<MesseClass> response) {
                    if (response.isSuccessful()) {
                        String str = response.body().notice.getAnswer();
                        if (!TextUtils.isEmpty(response.body().notice.getToken())) {
                            //Переход на след. активити и сохранение username
                            ed.putString("name", String.valueOf(log.getText()));
                            ed.commit();
                            Intent intent = new Intent(MainActivity.this, Main2Activity.class);
                            startActivity(intent);
                        } else if (!TextUtils.isEmpty(response.body().notice.getAnswer())) {
                            if (str.equals("User is active")) {
                                api.cal3(new Messe(String.valueOf(log.getText()))).enqueue(new Callback<MesseClass>() {
                                    @Override
                                    public void onResponse(Call<MesseClass> call, Response<MesseClass> response) {
                                        if (response.isSuccessful()) {
                                            api.cal(new Messe(String.valueOf(pas.getText()), String.valueOf(log.getText()))).enqueue(new Callback<MesseClass>() {
                                                @Override
                                                public void onResponse(Call<MesseClass> call, Response<MesseClass> response) {
                                                    if (response.isSuccessful()) {
                                                        if (!TextUtils.isEmpty(response.body().notice.getToken())) {
                                                            //Переход на след. активити и сохранение username
                                                            ed.putString("name", String.valueOf(log.getText()));
                                                            ed.commit();
                                                            Intent intent = new Intent(MainActivity.this, Main2Activity.class);
                                                            startActivity(intent);
                                                        } else {
                                                            Toast.makeText(MainActivity.this, "Error!", Toast.LENGTH_SHORT).show();
                                                        }
                                                    } else {
                                                        Toast.makeText(MainActivity.this, response.message(), Toast.LENGTH_SHORT).show();
                                                    }
                                                }

                                                @Override
                                                public void onFailure(Call<MesseClass> call, Throwable t) {
                                                    Toast.makeText(getApplicationContext(), t.getMessage(), Toast.LENGTH_LONG).show();
                                                }
                                            });
                                        } else {
                                            Toast.makeText(MainActivity.this, response.message(), Toast.LENGTH_SHORT).show();
                                        }
                                    }

                                    @Override
                                    public void onFailure(Call<MesseClass> call, Throwable t) {
                                        Toast.makeText(MainActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                                    }
                                });
                            } else {
                                Toast.makeText(MainActivity.this, response.body().notice.getAnswer(), Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            Toast.makeText(MainActivity.this, response.body().notice.getText(), Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        Toast.makeText(getApplicationContext(), response.message(), Toast.LENGTH_LONG).show();
                    }
                }

                @Override
                public void onFailure(Call<MesseClass> call, Throwable t) {
                    Toast.makeText(getApplicationContext(), t.getMessage(), Toast.LENGTH_LONG).show();
                }
            });

        }
    }
}
