package com.example.myapplication4;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class SomeAdapter extends RecyclerView.Adapter<SomeAdapter.VH> {

    private String[] list;

    //Конструктор, принимающий массив строк
    public SomeAdapter (String[] list) {
        this.list = list;
    }

    //90% из того, что тут написано - предлагается автоматом.

    //Создаём новый объект типа VH (класс написан ниже) и в качестве параметра отправляем вот эту всю огромную хрень в скобках
    @NonNull
    @Override
    public VH onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new VH(LayoutInflater.from(parent.getContext()).inflate(R.layout.item, parent, false));
    }

    //Тут мы задаём текст для нашей TextView'шки, обращаясь к list'у, который у мы получили в конструкторе (я сам не до конца знаю как это работает)
    @Override
    public void onBindViewHolder(@NonNull VH holder, int position) {
        holder.inf.setText(list[position]);
    }

    //Тупа отдаём ему размер нашего массива
    @Override
    public int getItemCount() {
        return list.length;
    }

    //Класс VH, в котором по умолчанию добавляется конструктор, в котором мы задаём айдишки
    public class VH extends RecyclerView.ViewHolder {
        TextView inf;
        public VH(@NonNull View itemView) {
            super(itemView);
            inf = itemView.findViewById(R.id.information);
        }
    }
}
